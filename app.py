import os
from flask import Flask, request, url_for, render_template
import easyocr

reader_ko = easyocr.Reader(['en', 'ko']) # this needs to run only once to load the model into memory
reader_ch = easyocr.Reader(['en', 'ch_sim'])
reader_ja = easyocr.Reader(['en', 'ja'])
#result = reader_ko.readtext('chinese.jpg')

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template('index.html')

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        file.save(os.path.join('static', file.filename))
        return dirtree()
    
    return '''
    <!doctype html>
    <form action="" method=post enctype=multipart/form-data>
         <input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''

@app.route("/list")
def dirtree():
    path = "static"
    return render_template('dirtree.html', tree=make_tree(path))

def make_tree(path):
    tree = dict(name=os.path.basename(path), children=[])
    try: lst = os.listdir(path)
    except OSError:
        pass #ignore errors
    else:
        for name in lst:
            fn = os.path.join(path, name)
            if os.path.isdir(fn):
                tree['children'].append(make_tree(fn))
            else:
                tree['children'].append(dict(name=name))
    return tree

@app.route("/translate", methods=['GET', 'POST'])
def transalte():
    filename = request.form['filename']
    language = request.form['language']
    result = ''
    print('=------' + language)
    if language == 'ko':
        result = reader_ko.readtext('static/' + filename, detail = 0)
    elif language == 'ch':
        result = reader_ch.readtext('static/' + filename, detail = 0)
    elif language == 'ja':
        result = reader_ja.readtext('static/' + filename, detail = 0)
         
    print(result)
    return render_template('result.html', src_image=filename, result=result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)